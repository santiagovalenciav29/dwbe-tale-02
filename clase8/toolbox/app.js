const express = require('express');
const app = express();
const sequelize = require('./db/db.js');
const User = require('./db/models/user.js');


const PORT = process.env.PORT || 3000;

app.get('/', (req,res) => {
    User.create({name:"Adriana",
                 birthday: new Date(1982, 09 , 15)
    }).then(user => {
    res.json(user);
    })

});

app.listen(PORT, () => {
    console.log(`arrancando el express desde el puerto ${PORT}`);

    sequelize.sync({ force: false }).then(() => {
        console.log("contentandonos a la base de datos ");
    }).catch(error => {
        console.log('no hemos podido establecer la conexion con la db', error);
    })

})