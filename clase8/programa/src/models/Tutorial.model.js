module.exports = ( sequelize, DataType ) => {
    const Tutorial = sequelize.define('tutoriales', {
        titulo: {
            type: DataType.STRING
        },
        descripcion: {
            type: DataType.STRING
        }
    });
    return Tutorial;
}