module.exports = (sequelize, DataTypes) => {
    const Comentario = sequelize.define("comentarios", {
        name: {
            type: DataTypes.STRING
        },
        texto: {
            type: DataTypes.STRING
        }
    });
    return Comentario;
}