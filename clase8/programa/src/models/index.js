const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('orm', 'orm', 'orm', {
    host:'localhost',
    dialect:'mysql'
});

const db = {};

//importacion
db.Sequelize = Sequelize;
//sequelize generado
db.sequelize = sequelize;

db.Tutoriales = require('./Tutorial.model')( sequelize, DataTypes);
db.Comentarios = require('./comentarios.model.js')(sequelize, DataTypes);

db.Tutoriales.hasMany(db.Comentarios);
db.Comentarios.belongsTo(db.Tutoriales);

module.exports = db;