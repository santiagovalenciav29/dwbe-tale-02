const express = require('express');
const app = express();
const db = require('./models');

app.use(express.json());

app.get ('/tutoriales', async (req,res) => {
    const tutoriales = await db.Tutoriales.findAll({
        include: ["comentarios"]
    });
    res.json(tutoriales);
})

app.post('/tutoriales', async (req,res) => {
    const nuevoTutorial = await db.Tutoriales.create({titulo: req.body.titulo, descripcion: req.body.descripcion});
    res.json(nuevoTutorial);
})

app.put('/actualizarTutorial/:id', async (req,res) => {
    const { id } = req.params;
    const resultado = await db.Tutoriales.update({titulo: req.body.titulo, descripcion : req.body.descripcion}, {
        where:{
            id
        }
    });
    res.json(resultado);
});

app.delete('/eliminarTutorial/:id', async(req,res) => {
    const { id } = req.params;
    const borrarTutorial = await db.Tutoriales.destroy({
        where :{
            id
        }
    });
    res.json(borrarTutorial);
})

app.get('/comentarios', async(req,res) => {
    const comentarios = db.Comentarios.findAll();
    res.json(comentarios);
})

app.post('/crearComentario', async(req,res) => {
    const { name, texto , tutorialeId} = req.body;
    const nuevoComentario = await db.Comentarios.create({ name, texto, tutorialeId});
    res.json(nuevoComentario);
})

app.delete('eliminarComentario/:id', async(req,res) => {
    const { id } = req.params;
    const comentarioEliminado = await db.Comentarios.destroy({
        where: {
            id
        }
    });
    res.json(comentarioEliminado);
})

//force false es para indicar que de no existir que cree la tabla
db.sequelize.sync({ force : false})
    .then(() => {
        console.log('conectando a la base de datos');
        app.listen(3000);
        console.log('vamos a darle ORM!!!');
    })
    .catch(error => {
        console.log(`error al conectarnos a la base de datos : ${error}`);
    });