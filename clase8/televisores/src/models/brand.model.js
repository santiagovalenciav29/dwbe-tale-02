module.exports = (sequelize, DataTypes) => {
    const Brand = sequelize.define("brands", {
        name : {
            type: DataTypes.STRING
        }
    });
    return Brand;
}
