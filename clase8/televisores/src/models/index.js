const { request } = require('express');
const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('Televisores', 'Televisores', 'Televisores', {
    host:'localhost',
    dialect:'mysql'
});

const db = {};

db.Sequelize = Sequelize;

db.sequelize = sequelize;


db.televisores = require('./televisores.model')(sequelize,DataTypes);
db.brands = require('./brand.model')(sequelize,DataTypes);
db.models = require('./models.model')(sequelize,DataTypes);

db.brands.hasMany(db.televisores);
db.televisores.belongsTo(db.brands);

db.models.hasMany(db.televisores);
db.televisores.belongsTo(db.models);

db.brands.hasMany(db.models);
db.models.belongsTo(db.brands);

module.exports = db;

