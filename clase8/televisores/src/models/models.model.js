module.exports = (sequelize, DataType) => {
    const Model = sequelize.define("models",{
        reference: {
            type:DataType.STRING
        }
    });
    return Model;
}