module.exports = (sequelize, DataType) => {
    const Tv = sequelize.define("Tvs",{
        price: {
            type: DataType.FLOAT
        },
        screen: {
            type: DataType.STRING
        },
        smart: {
            type: DataType.BOOLEAN
        },
        quantity: {
            type: DataType.INTEGER
        }
    });
    return Tv;
}