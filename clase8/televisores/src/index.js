const express = require('express');
const app = express();
const db = require('./models');
const { Op } = require("sequelize");

app.use(express.json());

app.post('/createBrand', async (req,res) => {
    const newBrand = await db.brands.create({name:req.body.name});
    res.json(newBrand);
})

app.get('/getBrands', async (req,res) => {
    const allBrands = await db.brands.findAll();
    res.json(allBrands);
})

app.post('/createModel', async (req,res) => {
    const newModel = await db.models.create({reference: req.body.reference});
    res.json(newModel)
})

app.get('/getModels', async (req,res) => {
    const getModel = await db.models.findAll();
    res.json(getModel)
})

app.put('/updatemodel/:id', async (req, res) => {
    const { id } = req.params;
    const updatedModel = await db.models.update({ reference : req.body.reference,
    brandId : req.body.brandId}, { where:{
        id
    }});
    res.json(updatedModel);
})

app.post('/createTv', async(req,res) => {
    const newTv = await db.televisores.create({
        price:req.body.price,
        screen:req.body.screen,
        smart: req.body.smart,
        quantity: req.body.quantity,
        brandId: req.body.brandId,
        modelId:req.body.modelId
    });
    res.json(newTv);
})

app.get('/getTvs', async(req,res) => {
    const getTvs = await db.televisores.findAll();
    res.json(getTvs);
})

app.get('/getModelsOfBrands/:id', async (req, res)=> {
    const { id } = req.params;
    const getBrand = await db.brands.findOne({ where: {
        id
    }});
    console.log(getBrand.dataValues);
    const models = await db.models.findAll({where : {
        brandId:parseInt(getBrand.dataValues.id)
    }});
    res.json(models)
})

app.get('/getTvsPrice/:price', async(req,res) => {
    const { price } = req.params;
    const getTvs = await db.televisores.findAll({where: {price:{
        [Op.lt]: price
    }}});
    console.log(getTvs)
    res.json(getTvs);
})

app.get('/getTvsPriceDescending/:price', async(req,res) => {
    const { price } = req.params;
    const getTvs = await db.televisores.findAll({where: {price:{
        [Op.gt]: price
    }}});
    console.log(getTvs)
    res.json(getTvs);
})

app.get('/getTvsOrder', async(req,res) => {
    const GetTvsOrder = await db.televisores.findAll({order: [["price","ASC"]]})
    res.json(GetTvsOrder);
});


db.sequelize.sync({ force: false })
    .then(() => {
        console.log('conectando a la base de datos Televisores');
        app.listen(3000);
        console.log('iniciando express');
    })
    .catch(error => {
        console.log(`tenemos que corregir algo para conectarnos revisa esto ${error}`)
    });