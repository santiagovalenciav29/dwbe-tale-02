const express = require('express');
const { QueryTypes } = require('sequelize');
const sequelize = require('./db.js');
const app = express();
app.use(express.json());

app.get('/discos', async(req,res) => {
    const discos = await sequelize.query(" SELECT * FROM disc",
    {
        type: QueryTypes.SELECT
    });
    res.json(discos)
});

app.post('/discos', async(req,res) => {
    const { nombre, banda, fecha_creacion } = req.body;
    const nuevoDisco = await sequelize.query("INSERT INTO disc (nombre, banda, fecha_creacion ) VALUES (?,?,?)",
    {
        replacements: [nombre, banda, fecha_creacion],
        type: QueryTypes.INSERT
    });
    res.status(201).send(nuevoDisco)
});

app.put('/discos/:id', async(req,res) => {
    const { id } = req.params;
    const {nombre, banda, fecha_creacion } = req.body;
     await sequelize.query("UPDATE disc SET nombre = ?, banda = ?, fecha_creacion = ? WHERE id=?",
    {
        replacements: [nombre, banda, fecha_creacion, id],
        type: QueryTypes.UPDATE
    });
    res.status(201).json("producto actualizado");
});

app.delete('/discos/:id', async(req,res) => {
    const { id } = req.params;
    await sequelize.query("DELETE FROM disc WHERE id = ?",
    {
        replacements: [id],
        type: QueryTypes.DELETE
    });
    res.json("producto borrado")
});

app.listen(5000);
console.log('vamos con discos desde el puerto 5000 !!!! ');