const { Sequelize } = require('sequelize');
const sequelize = new Sequelize( 'lyrics', 'lyrics', 'lyrics', {
    host: 'localhost',
    dialect: 'mysql'
});

(async () => {
    try{
        await sequelize.authenticate();
        console.log('connection is running from Mysql');
    } catch{
        console.error('unable connection from Mysql')
    }
})();

module.exports = sequelize;