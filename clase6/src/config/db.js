const Sequalize = require('sequelize');
const conf = require('./index.js');

const db = new Sequalize(conf.database, conf.username, conf.password, {
    host: conf.host,
    dialect: conf.dialect,
    operatorsAliases: false,

    pool : {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

module.exports = db;