const Sequalize = require('sequelize');

const db = require('../config/db.js');

const Lyric = db.define('lyrics', {
    tittle: {
        type: Sequalize.STRING
    },
    singer: {
        type: Sequalize.STRING
    },
    gender: {
        type: Sequalize.STRING
    },
})

module.exports = Lyric
