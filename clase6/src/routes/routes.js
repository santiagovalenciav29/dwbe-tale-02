const express = require('express');
const router = express.Router();
const db = require('../config/db.js');
const Lyric = require('../models/lyrics.js');

router.get('/', (req,res) => 
    Lyric.findALL()
        .then(lyrics =>{
            console.log(lyrics);
            res.sendStatus(200);
        })
        .catch(err => console.log(err)));

module.exports = router;