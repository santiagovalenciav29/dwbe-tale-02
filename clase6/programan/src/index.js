const express = require('express');
const { QureyTypes, QueryTypes } = require('sequelize');
const sequelize = require('./db.js');
const app = express();
app.use(express.json());

app.get('/cantante/:id', async (req,res) => {
    const { id } = req.params;
    const cantante = await sequelize.query("SELECT * FROM singers WHERE id = ?",
    {
        replacements: [id],
        type: QueryTypes.SELECT
    });

    res.json(cantante)
})

app.post('/cantante', async(req,res) => {
    const { nombre, banda, nacionalidad } = req.body;
    const nuevoCantante = await sequelize.query("INSERT INTO singers (nombre, banda, nacionalidad ) VALUES (?,?,?)",
        {
            replacements: [nombre, banda, nacionalidad],
            type:QueryTypes.INSERT
        }
    );
    res.status(201).send(nuevoCantante)
});

app.put('/cantante/:id', async(req,res) => {
    const { nombre, banda, nacionalidad } = req.body;
    const { id } = req.params;
    await sequelize.query("UPDATE singers SET nombre= ?, banda = ?, nacionalidad = ? WHERE id = ? ",
    {
        replacements : [nombre, banda, nacionalidad, id],
        type : QueryTypes.UPDATE
    });
    res.status(201).send('usuario actualizado')

});

app.delete('/cantante/:id', async(req,res) => {
    const { id } = req.params;
    await sequelize.query("DELETE FROM singers WHERE id= ?",
    {
        replacements: [id],
        type: QueryTypes.DELETE
    });
    res.status(200).json('Usuario eliminado')
})


app.listen(3000);
console.log('escuchando en el puerto 3000');

