const sayHello = () => {
    return 'hello world'
}

const add = (a, b) => {
    return a + b;
}

module.exports = { sayHello, add}