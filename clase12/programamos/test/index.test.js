const chai = require('chai');

const app = require('../index.js');

describe('test para index y sus funciones', () => {
    before(() => { console.log('before testing run')});
    after(() => { console.log('efter testing run ')})
    it('the sayHello function must return hello world', () => {
        const result = app.sayHello();
        chai.assert.equal(result, 'hello world');
    });

    it('the add function must return 4 when his parameters are 2 and 2 ', () => {
        const result = app.add(2,2);
        chai.assert.equal(result, 4);
    });

    it('the add function must return a number ', () => {
        const result = app.add(2,2);
        chai.assert.typeOf(result, 'number');
    });
});