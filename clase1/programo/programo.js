const fetch = require('node-fetch');

let promesa1 = new Promise((resolve, reject) => {
    setTimeout(() => {
       resolve('esperando el set, vamos!!!!')
    }, 5000);

    setTimeout(() => {
        reject('probando los fallos, vamos!!!!')
     }, 3000);
});

promesa1
    .then((mensaje) => {console.log(mensaje)})
    .catch((mensaje) => {console.error(mensaje)});


console.log('primero vamos con esta tarea');

let promesa2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('ahora viene la promesa 2')
     }, 1000);
})

promesa2
    .then((msj) =>{
        console.log(msj)
    })

fetch('https://dog.ceo/api/breeds/image/random')
    .then((res) => {
        return res.json();
    })
    .then((json) => {
        console.log(json);
    })
    .catch(err => { console.log(err); });