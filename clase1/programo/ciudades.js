const fetch = require('node-fetch');
const ciudades = ["barranquilla", "cartagena", "medellin", "bogota", "cali", "bucaramanga", "pasto"];

for ( i = 0 ; i < 3 ; i ++){
    const aleatorio = Math.floor(Math.random() * ciudades.length);
    const cityName = ciudades[aleatorio];
    const buscarCiudad = (cityName) => {
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=df774efd6587837c8e5ffd97389ea59e`)
            .then ( res => res.json())
            .then (json => {
                const main = json.main;
                    console.log(json.name)
                    console.log(`${parseInt(main.temp - 273.15 )} grados celsius`);
            })
            .catch(error => console.log(error));
    }
    buscarCiudad(cityName);
};

