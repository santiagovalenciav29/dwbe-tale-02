const fetch = require('node-fetch');

const buscarSeguidores = (nombreUsuario) => {
    fetch(`https://api.github.com/users/${nombreUsuario}/followers`)
        .then ( response => response.json())
        .then (res => {
            for (let index = 0; index < res.length && index < 5; index++){
                const seguidor = res[index];
                console.log(seguidor);
            }
        })
        .catch(error => console.log(error));
}

buscarSeguidores("maosierra");