const { taskOne, taskTwo } = require('./tasks.js');


async function main() {

    try {

        console.time('measuring time');
        const results = await Promise.all([taskOne(), taskTwo()])
        // const valueOne = await taskOne();
        // const valueTwo = await taskTwo();
        console.timeEnd('measuring time');
        
        console.log('Task one returned', results[0]);
        console.log('Task two returned', results[1]);
    } catch(e) {
        console.log(e);
    }

};

main();