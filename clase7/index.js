const sequelize = require('./db.js');
const { QueryTypes } = require('sequelize');
const express = require('express');
const app = express();
app.use(express.json());


app.get('/bandas', async (req,res) => {
    const bandas = await sequelize.query(`SELECT * FROM bandas_musicales`,
    {
        type: QueryTypes.SELECT
    });
    res.json(bandas);
});

app.get('/bandas/:id', async (req,res) => {
    const { id } = req.params;
    const ObtenerAlbumBanda = await sequelize.query(`SELECT albunes.nombre_album AS 'nombreAlbum',
    albunes.id AS 'album id',
    albunes.fecha_publicacion AS 'fecha publicacion album' FROM albunes
    INNER JOIN canciones
    ON albunes.banda = canciones.banda WHERE albunes.id = ?`,
    {
        replacements: [id],
        type: QueryTypes.SELECT
    });
    res.json(ObtenerAlbumBanda)
});

app.get('/cancion/:id', async(req, res) => {
    const { id } = req.params;
    const obtenerCanciones = await sequelize.query(`SELECT * FROM canciones WHERE id = ?`,
    {
        replacements: [id],
        type: QueryTypes.SELECT
    });
    res.json(obtenerCanciones)
});

app.listen(3000)
console.log('here we go since 3000 port !!!');