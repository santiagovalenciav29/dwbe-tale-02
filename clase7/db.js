const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('programo','programo','programo', {
    host: "localhost",
    dialect: 'mysql'
});

(async () => {
    try {
        await sequelize.authenticate();
        console.log('running with mysql');
    } catch (error) {
        console.error('something bad happen ')
    }
})();

module.exports = sequelize;