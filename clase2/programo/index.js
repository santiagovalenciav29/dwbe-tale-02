const fetch = require('node-fetch');

const url = `https://api.openweathermap.org/data/2.5/weather?q=medellin&appid=df774efd6587837c8e5ffd97389ea59e`

// function climaCiudad(cityName) {
//     fetch(url)
//     .then (respuesta => respuesta.json())
//     .then (resultado => console.log(resultado))
//     .catch(err => console.error(err));
// }

// climaCiudad();

async function climaCiudadAsync() {
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        console.log(resultado);
    } catch (error) {
        console.error(error);
    }
}

climaCiudadAsync()
    .then(clima => {
        console.log(clima);
    });

(async function() {
    const clima = await climaCiudadAsync();
    console.log(clima);
})();