const fetch = require('node-fetch');
const express = require('express');
const app = express();

app.use(express.json());

const api_key = "82164e92";
const url = `http://www.omdbapi.com/?apikey=${api_key}&`;

app.get('/peliculas', async (req,res) => {
    const { nombrePelicula } = req.query;
    const respuesta = await fetch(url +`t=${nombrePelicula}`);
    const pelicula = await respuesta.json();
    res.json({
        titulo: pelicula.Title,
        imagen: pelicula.Poster,
        description: pelicula.Plot
    });
});

app.post('/peliculas', async (req, res) => {

    const PromesaPeliculas = async(nombrePelicula) => {
        const respuesta = await fetch(url + `t=${nombrePelicula}`);
        const pelicula = await respuesta.json();
        return {
            titulo: pelicula.Title,
            imagen: pelicula.Poster,
            description: pelicula.Plot
        };
    }
        const { peliculas } = req.body;
        const promesas = peliculas.map(pelicula => PromesaPeliculas(pelicula));
        const listadoPeliculas = await Promise.all(promesas)
        res.json({ listadoPeliculas });
});

app.listen(3000);
console.log('dandole desde el 3000, vamos !!!!')