const express = require('express');
const fetch = require('node-fetch');
const redis = require('redis');
const app = express();

const clienteRedis = redis.createClient(6379);

app.use(express.json());

const obtenerComentario = async(req,res) => {
    const { id } = req.params;
    const respuesta = await fetch('https://jsonplaceholder.typicode.com/comments/' + id);
    const dato = await respuesta.json();
    clienteRedis.setex('comentario_' + id, 200, JSON.stringify(dato));
    res.json(dato);
};

const cache = (req,res, next) => {
    const { id } = req.params;
    clienteRedis.get('comentario_' + id, (err, dato) => {
        if(err) throw err;
        if(dato){
            res.json(JSON.parse(dato));
        }else {
            next();
        }
    })
}

app.get('/comentarios/:id', cache, obtenerComentario);




app.listen(3000);
console.log('escuchando en el puerto 3000');
