const express = require('express');
const app = express();
require('./db.js');

const Cuenta = require ('./models/modelo.cuenta.js');

app.use(express.json());

app.use('/cuenta', require('./routes/rutas.cuenta.js'));

app.listen(3000);

console.log('escuchando puerto 3000');