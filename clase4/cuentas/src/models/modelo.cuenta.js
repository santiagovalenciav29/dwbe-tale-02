const mongoose = require('mongoose');

const documentoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required:true
    },
    email: {
        type: String,
        required: true,
    },
    saldoInicial: {
        type: Number,
        default: 0
    },
    saldoActual: {
        type: Number,
        default:0
    }
});

module.exports = mongoose.model('Cuenta', documentoSchema);