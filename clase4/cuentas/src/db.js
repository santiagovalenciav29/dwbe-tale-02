const mongoose = require('mongoose');

(async () => {
    const db = await mongoose.connect('mongodb://localhost:27017/cuentaBancaria',
    { useNewUrlParser: true, useUnifiedTopology:true});

    console.log('conectado a mongoDB')
})()