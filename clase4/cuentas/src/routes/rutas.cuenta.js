const express = require('express');
const routes = express.Router();
const mongoose = require('mongoose');
const Cuenta = require('../models/modelo.cuenta.js');
require('../db.js');

routes.get('/obtenerCuenta', async (req,res) => {
    const cuenta = await Cuenta.find();
    res.json(cuenta);
})

routes.post('/crearCuenta', async( req,res) => {
    const { nombre, apellido, email, saldoInicial} = req.body;
    const existeCuenta = await Cuenta.findOne({ email });
    if(existeCuenta){
        res.status(400).json('correo en uso');
    } else {
        const nuevaCuenta = new Cuenta ( { nombre, apellido, email, saldoInicial });
        nuevaCuenta.save();
        res.json(nuevaCuenta);
    }
})

routes.put('/actualizarSaldo/:email', async(req,res) => {
    const {saldoActual} = req.body;
    const existeCuenta = await Cuenta.findOne({ email:req.params.email });
    if(existeCuenta){
        existeCuenta.saldoActual = existeCuenta.saldoInicial + existeCuenta.saldoActual + saldoActual;
        existeCuenta.save();
        res.json(existeCuenta)
    } else{
        res.status(400).json('correo invalido')
    }
})

routes.put('/transferir/emailOrigen/:emailO/emailDestino/:emailD', async (req,res) => {
    const {saldoActual} = req.body;
    const existeCuentaOrigen = await Cuenta.findOne({ email:req.params.emailO});
    console.log(existeCuentaOrigen);
    const existeCuentaDestino = await Cuenta.findOne({ email:req.params.emailD});
    console.log(existeCuentaDestino);
    if(existeCuentaOrigen && existeCuentaDestino && existeCuentaOrigen.saldoActual + existeCuentaOrigen.saldoInicial >= saldoActual ){
        existeCuentaOrigen.saldoActual = existeCuentaOrigen.saldoActual - saldoActual;
        existeCuentaDestino.saldoActual = existeCuentaDestino.saldoActual + saldoActual;
        existeCuentaOrigen.save();
        existeCuentaDestino.save();
        res.json(existeCuentaOrigen)
    } else {
        res.status(400).json('saldo insuficiente')
    }
})


module.exports = routes;