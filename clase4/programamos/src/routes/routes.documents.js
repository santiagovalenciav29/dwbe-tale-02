const express = require('express');
const routes = express.Router();
const mongoose = require('mongoose');
const DocumentoPersonal = require('../models/DocumentoPersonal.js');
require('../db.js');

routes.get('/obtenerdocumento', async (req,res) => {
    const documentos = await DocumentoPersonal.find();
    res.json(documentos);
})

routes.post('/documentoPersonal', async (req,res) => {
    const { nombre, apellido, email } = req.body;
    const existeDocumento = await DocumentoPersonal.findOne({ email });
    if(existeDocumento){
        res.status(400).json('el correo ya existe');
    } else {
        const nuevoDocumento = new DocumentoPersonal ( { nombre, apellido, email });
        nuevoDocumento.save();
        res.json(nuevoDocumento);
    }
});

routes.post('/agregarContacto/:email', async (req, res) => {
    const { email } = req.params;
    const { nombre, telefono, redesSociales, direccion } = req.body;
    const documentoExistente = await DocumentoPersonal.findOne ({ email });
    if (documentoExistente) {
        documentoExistente.contactos.push( { nombre, telefono, redesSociales, direccion });
        documentoExistente.save();
        res.json(documentoExistente);
    } else {
        res.status(400).json('el correo no existe');
    }
});


module.exports = routes;