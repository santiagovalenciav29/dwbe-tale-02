const express = require('express');
const app = express();
require('./db.js');

const DocumentoPersonal = require('./models/DocumentoPersonal.js');

app.use(express.json());

app.use('/documentos', require('./routes/routes.documents.js'))

app.listen(3000);
console.log('escuando puerto 3000');