const mongoose = require('mongoose');

(async () => {

    const mongo = await mongoose.connect('mongodb://localhost:27017/product', {useNewUrlParser: true, useUnifiedTopology: true});
    
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('poniendo a correr mongo desde node, vamos!!!')
    });
    
    const usuarioSchema = new mongoose.Schema({
        nombre: {
            required: true,
            type:String
                },
        apellido:{
            required: true,
            type:String
                },
        direccion:{
            required: true,
            type:String
                 },
        edad:{
            required: true,
            type: Number
            },
        isAdmin:{
            type:Boolean,
            default:false
        }
    });
    
    const Usuario = mongoose.model('Usuarios', usuarioSchema);
    
    // creacion del usuario
    // const usuarioNuevo = new Usuario({nombre:"Emilio", apellido:"Valencia",
    // direccion:"Avenida siempre viva", edad: 84, isAdmin:false });
    
    // await usuarioNuevo.save();
    // console.log(usuarioNuevo);

    // buscar todos los usuarios del sistema
    const usuarios = await Usuario.find({isAdmin:false});
    console.log(usuarios);

    //actualizar usuarios
    // const usuario = await Usuario.findById('60fa09093869cc47ec99e199');
    // usuario.nombre = "nombre Prueba";
    // usuario.edad = 4;
    // usuario.save();
    // console.log(usuario)

    // eliminar usuario
    // const result = await Usuario.findByIdAndDelete('60fa09093869cc47ec99e199');

})();
