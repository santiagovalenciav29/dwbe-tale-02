const mongoose = require('mongoose');

    const platoSchema = new mongoose.Schema({
        nombre: {
            required: true,
            type:String
                },
        precio : {
            required: true,
            type:Number
                },
        categoria:{
            required: true,
            type:String
                 },
    
    });

    const plato = mongoose.model('Plato', platoSchema);


module.exports = plato
