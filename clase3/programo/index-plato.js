const mongoose = require('mongoose');
const express = require ('express');
const app = express();

app.use(express.json());

(async () => {
    const plato = await require('./schema/schema.js')
    
    const mongo = await mongoose.connect('mongodb://localhost:27017/Menu', {useNewUrlParser: true, useUnifiedTopology: true});
    
    const db = await mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('poniendo a correr mongo desde node, vamos!!!')
    });

    // creacion del plato
    // const platoNuevo1 = new plato({nombre:"salchipapa", precio: 9000,
    // categoria: "comida rapida"});

    // await platoNuevo1.save();
    // console.log(platoNuevo1);

    // const platoNuevo2 = new plato({nombre:"platano con queso", precio: 3500,
    // categoria: "comida rapida"});

    // await platoNuevo2.save();
    // console.log(platoNuevo2);
    app.get('/obtenerPlatos', async (req,res) => {
        const platos = await plato.find();
        res.json(platos);
    })

    app.post('/crearPlato', async (req,res) => {
        const { nombre, precio, categoria} = req.body;
        const platoNuevo = await new plato({nombre: nombre, precio: precio,categoria: categoria});
        await platoNuevo.save();
        res.json(platoNuevo);
    })

    app.put('/actualizarPlato/:id', async(req,res) =>{
        const platillo = await plato.findById({_id:req.params.id});
        platillo.nombre = req.body.nombre;
        platillo.precio = req.body.precio;
        platillo.save();
        res.json(platillo);
    })

    app.delete('/borrarPlato/:id', async(req,res) => {
        const PlatoBorrar = await plato.findByIdAndDelete({_id:req.params.id});
        res.json(await plato.find())
    })
    // console.log(platos);
    app.listen(3000, () => {console.log('escuchando en el puerto ' + 3000)})
})();