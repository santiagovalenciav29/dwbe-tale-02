const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/product', {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
 console.log("we are connected!!!")
});

const productSchema = new Schema({
    productName: String,
    price: Number,
})

const Products = mongoose.model("Products", productSchema);

const getProducts = () => Products.find({});

const getProductById = (productId) => Products.find({ _id : productId});
